import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BarService {
  private myBaz = 'baz';

  constructor() {
  }

  get baz(): Observable<string> {
    return of(this.myBaz);
  }
}
