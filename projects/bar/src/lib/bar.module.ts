import {NgModule} from '@angular/core';
import {BarComponent} from './bar.component';

@NgModule({
  declarations: [BarComponent],
  imports: [],
  exports: [BarComponent]
})
export class BarModule {
}
