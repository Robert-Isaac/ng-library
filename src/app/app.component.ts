import {Component} from '@angular/core';
import {BarService} from 'bar';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'foo';
  baz$ = this.barService.baz;

  constructor(
    private barService: BarService
  ) {
  }
}
